<?php

return [
    'current_app_id' => env('OPENEXCHANGERATES_APP_ID'),
    'reply_markup_button' => [
        [
            '✅丝绸公群导航',
            '💹交易记录查询',
        ],
        [
            '✅丝绸工作人员名单',
            '🛎钱包交易提醒'
        ],
    ],
    'welcome_text' => <<<EOF
    🛎点击钱包交易提醒—绑定地址后，钱包余额发生变动机器人会主动提醒。

🛎点击交易记录查询—输入地址后，机器人会反馈该地址近期交易记录。

✅以上数据来源于波场区块链浏览器-TRONSCAN
          丝绸仅为数据采集方！

❤️建议每位用户钱包/网站/机器人3重验证，以完全杜绝不必要的损失。
EOF,
    'button' => [
        'binding' => '✅',
        'unbind' => '☑️',
    ],

    'binding_text' => <<<EOF
    🛎个人钱包地址

EOF,

    'binding_options' => [
        'unbind' => 'd',
        'income' => 's',
        'expenses' => 'e',
        'balance' => 'b',
    ],

    'options_map' => [
        'unbind' => '解绑',
        'income' => '收入通知',
        'expenses' => '支出通知',
        'balance' => '余额通知',
    ],

    'default_options' => [
//        'unbind',
        'income' => 1,
        'expenses' => 1,
        'balance' => 1,
    ],

    'send_binding_text' => '✅请直接发送您的需要监控交易的trc20地址',
    'send_query_text' => '✅请直接发送您需要查询的trc20地址',
];
