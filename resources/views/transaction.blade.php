<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>列表页面</title>
    <!-- 引入Bootstrap的CSS文件 -->
    <link rel="stylesheet" href="https://cdn.bootcdn.net/ajax/libs/bootstrap/5.0.0-beta2/css/bootstrap.min.css">
    <!-- 引入 flatpickr 的 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
</head>
<body>
<div class="container my-3">
    <div class="row">
        <div class="col">
            <form action="{{ route("trc20.transaction") }}" method="get">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="address" class="form-label">地址</label>
                            <input type="text" id="address" name="address" class="form-control"
                                   value="{{ $input['address'] ?? '' }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="start-time" class="form-label">开始时间</label>
                            <input type="text" id="start-time" name="start_time" class="form-control"
                                   value="{{ $input['start_time'] ?? '' }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="end-time" class="form-label">结束时间</label>
                            <input type="text" id="end-time" name="end_time" class="form-control"
                                   value="{{ $input['end_time'] ?? '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="page_size" class="form-label">每页数量</label>
                            <input type="text" id="page_size" name="page_size" class="form-control"
                                   value="{{ $input['page_size'] ?? 100 }}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <button type="submit" id="search-btn" class="btn btn-primary mt-3">搜索</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col">
            <table class="table">
                <thead class="table-dark">
                <tr>
                    <th>转出</th>
                    <th>转入</th>
                    <th>金额</th>
                    <th>类型</th>
                    <th>交易时间</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($items as $item)
                    <tr class="list-group-item-action">
                        <td>{{ $item['from'] }}</td>
                        <td>{{ $item['to'] }}</td>
                        <td>{{ $item['value'] }}</td>
                        <td class="@if($item['income']) text-danger @else text-success @endif">@if($item['income'])
                                收入
                            @else
                                支出
                            @endif</td>
                        <td>{{ $item['time'] }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center">
                    {{--                    <li class="page-item"><a class="page-link" href="#">上一页</a></li>--}}
                    @foreach($input['pages'] as $key => $value)
                        <li class="page-item @if($input['page'] - 1 == $key) active else  @endif"><a class="page-link" href="{{ $value }}">{{ ++$key }}</a></li>
                    @endforeach
                    {{--                    <li class="page-item active"><a class="page-link" href="#">1</a></li>--}}
                    {{--                    <li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                    {{--                    <li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                    {{--                    <li class="page-item"><a class="page-link" href="#">下一页</a></li>--}}
                </ul>
            </nav>

        </div>
    </div>
</div>

<!-- 引入 flatpickr 的 JavaScript 文件 -->
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    flatpickr("#start-time", {
        enableTime: true,
        dateFormat: "Y-m-d H:i:S"
    });
    flatpickr("#end-time", {
        enableTime: true,
        dateFormat: "Y-m-d H:i:S"
    });
</script>

<!-- 引入Bootstrap的JavaScript文件 -->
<script src="https://cdn.bootcdn.net/ajax/libs/bootstrap/5.0.0-beta2/js/bootstrap.min.js"></script>
</body>
</html>
