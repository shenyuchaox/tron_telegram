<?php

namespace App\Console\Commands;

use App\Models\TronTelegramUser;
use App\Services\TelegramService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class TradingNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trading:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '交易通知';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tronAddress = (new TronTelegramUser())->newQuery()
            ->groupBy("tron_address")
            ->select(['tron_address'])
            ->pluck("tron_address")
            ->toArray();
        $telegramService = app()->make(TelegramService::class);
        foreach ($tronAddress as $item) {
            $telegramService->sendUSDTEvent($item);
        }
    }
}
