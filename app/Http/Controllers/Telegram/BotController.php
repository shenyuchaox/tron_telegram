<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Services\TelegramService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Laravel\Facades\Telegram;

class BotController extends Controller
{
    public function callback(TelegramService $service)
    {
        try {
            $bot = Telegram::bot(null);
            $update = $bot->getWebhookUpdate();
            $log = Log::channel('telegram');
            $log->info($update->toJson());
            $service->handle($update);
        } catch (\Throwable $throwable) {
            dd($throwable);
        }
    }
}
