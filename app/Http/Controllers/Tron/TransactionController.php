<?php

namespace App\Http\Controllers\Tron;

use App\Http\Controllers\Controller;
use App\Services\TronService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TransactionController extends Controller
{
    public function index(Request $request, TronService $service)
    {
        $address = $request->query("address", "");
        $start = $request->query("start_time");
        $end = $request->query("end_time");
        $page = $request->query('page', 1);
        $pageSize = $request->query('page_size', 100);
        $input = $request->all();
        $input['page'] = $input['page'] ?? 1;
        $options = [];
        if ($start) {
            $options['min_timestamp'] = Carbon::parse($start, "Asia/Shanghai")->getPreciseTimestamp(3);
        } else {
            $options['min_timestamp'] = 0;
        }
        if ($end) {
            $options['max_timestamp'] = Carbon::parse($end, "Asia/Shanghai")->getPreciseTimestamp(3);
        } else {
            $options['max_timestamp'] = now('Asia/Shanghai')->getPreciseTimestamp(3);
            $input['end_time'] = now('Asia/Shanghai')->format('Y-m-d H:i:s');
        }
        $options['contact_address'] = 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t';
        $key = md5($address . $start . $end);
        $items = Cache::get($key);
        if (!$items) {
            $data = $service->getTrc20TransactionList($address, $options);
            $items = [];
            foreach ($data as $item) {
                if ($item['token_info']['address'] != "TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t") {
                    continue;
                }
                $item['value'] = TronService::decimalValue($item['value'], $item['token_info']['decimals']);
                $item['time'] = Carbon::createFromTimestampUTC($item['block_timestamp'] / 1000)->tz('Asia/Shanghai')->format("Y-m-d H:i:s");
                if ($address == $item['from']) {
                    $item['income'] = 0;
                } else {
                    $item['income'] = 1;
                }
                $items[] = $item;
            }
            Cache::put($key, $items, 3600);
        }
        $pages = [];
        $pageNum = ceil(count($items) / $pageSize);
        $pageNum = $pageNum > 30 ? 30 : $pageNum;
        for ($i = 1; $i <= $pageNum; $i++) {
            $pages[] = route('trc20.transaction') . "?" . http_build_query([
                    'address' => $input['address'] ?? '',
                    'start_time' => $input['start_time'] ?? '',
                    'end_time' => $input['end_time'] ?? '',
                    'page_size' => $input['page_size'] ?? 100,
                    'page' => $i,
                ]);
        }
        $input['pages'] = $pages;
        $items = array_slice($items, ($page - 1) * $pageSize, $pageSize);

        return view("transaction", compact('items', 'input'));
    }
}
