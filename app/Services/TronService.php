<?php

namespace App\Services;

use Comely\DataTypes\BcNumber;
use IEXBase\TronAPI\Provider\HttpProvider;
use IEXBase\TronAPI\Tron;

class TronService
{
    /**
     * @var Tron
     */
    private $tron;

    public function __construct()
    {
        $fullNode = new HttpProvider('https://api.trongrid.io');
        $solidityNode = new HttpProvider('https://api.trongrid.io');
        $eventServer = new HttpProvider('https://api.trongrid.io');
        $this->tron = new Tron($fullNode, $solidityNode, $eventServer);
    }

    public function getTrc20Transactions(string $address, array $options)
    {
        return $this->tron->getManager()->request(
            "v1/accounts/{$address}/transactions/trc20?" . http_build_query($options), [], "get"
        );
    }

    public function getTrc20TxInfo(string $address, array $options)
    {
        $count = 0;
        $expenses = 0;
        $income = 0;
        $fingerprint = '';
//        $now = now("Asia/Shanghai")->getPreciseTimestamp(3);
        $options['limit'] = 200;
        for ($i = 0; $i < 5; $i++) {
            $options['fingerprint'] = $fingerprint;
            $result = $this->getTrc20Transactions($address, $options);
            $count += count($result['data'] ?? []);
            foreach ($result['data'] ?? [] as $item) {
                if ($item['from'] == $address) {
                    $expenses = bcadd($expenses, $this->decimalValue($item['value'], $item['token_info']['decimals']), $item['token_info']['decimals']);
                } else {
                    $income = bcadd($income, $this->decimalValue($item['value'], $item['token_info']['decimals']), $item['token_info']['decimals']);
                }
            }
            if (empty($result['meta']['fingerprint'])) {
                break;
            }
            $fingerprint = $result['meta']['fingerprint'];
        }
        return [
            'count' => $count,
            'income' => $income,
            'expenses' => $expenses,
        ];
    }


    public function getTrc20TransactionList(string $address, array $options)
    {
        $fingerprint = '';
        $limit = $options['limit'] ?? 200;
        $options['limit'] = $limit;
        $page = ceil(200 / $limit);
        $list = [];
        for ($i = 0; $i < $page; $i++) {
            $options['fingerprint'] = $fingerprint;
            $result = $this->getTrc20Transactions($address, $options);
            array_push($list, ...$result['data']);
            if (empty($result['meta']['fingerprint'])) {
                break;
            }
            $fingerprint = $result['meta']['fingerprint'];
        }
        return $list;
    }

    public static function decimalValue(string $int, int $scale = 18): string
    {
        return (new BcNumber($int))->divide(pow(10, $scale), $scale)->value();
    }
}
