<?php

namespace App\Services;

use AmrShawky\LaravelCurrency\Facade\Currency;
use App\Models\TronTelegramUser;
use Carbon\Carbon;
use GuzzleHttp\Client;
use IEXBase\TronAPI\Provider\HttpProvider;
use IEXBase\TronAPI\Tron;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Telegram\Bot\Keyboard\Button;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Objects\Message;
use Telegram\Bot\Objects\Update;
use Telegram\Bot\Objects\Update as UpdateObject;

class TelegramService
{
    /**
     * @var \Telegram\Bot\Api
     */
    private $bot;
    /**
     * @var Tron
     */
    private $tron;
    /**
     * @var TronTelegramUser
     */
    private $model;

    public function __construct()
    {
        $fullNode = new HttpProvider('https://api.trongrid.io');
        $solidityNode = new HttpProvider('https://api.trongrid.io');
        $eventServer = new HttpProvider('https://api.trongrid.io');
        $this->bot = Telegram::bot(null);
        $this->tron = new Tron($fullNode, $solidityNode, $eventServer);
        $this->model = new TronTelegramUser();
    }

    public function handle(UpdateObject $update)
    {
        if ($update->isEmpty()) {
            return;
        }
        switch ($update->objectType()) {
            case "message":
                $message = $update->getMessage();
                if ($this->handleAddressMessage($update)) {
                    return;
                }

                if ($message->hasCommand()) {
                    $this->handleCommand($update);
                    return;
                }
//                if ($message->replyToMessage) {
//                    $this->handleReplyMessage($update);
//                    return;
//                }
                $this->handleText($update);
                break;
            case "callback_query":
                $this->handleCallbackQuery($update);
                break;
            default:
                break;
        }
    }

    public function getBindAddressCacheKey(Update $update)
    {
        if ($update->objectType() == "message") {
            $fromId = $update->message->from->id;
        } else {
            $fromId = $update->callbackQuery->from->id;
        }

        return "bind_address_" . $fromId;
    }

    public function getQueryAddressCacheKey(Update $update)
    {
        if ($update->objectType() == "message") {
            $fromId = $update->message->from->id;
        } else {
            $fromId = $update->callbackQuery->from->id;
        }

        return "query_address_" . $fromId;
    }

    public function handleCallbackQuery(Update $update)
    {
        $data = $update->callbackQuery->data;
        switch ($data) {
            case "delete":
                $this->deleteMessage($update);
                break;
            case "binding":
                $this->sendBinding($update);
                break;
            case 'back':
                $this->sendTgTronList($update);
                break;
            case 'delete_bind':
                Cache::forget($this->getBindAddressCacheKey($update));
                $this->deleteMessage($update);
                break;
            case 'delete_query':
                Cache::forget($this->getQueryAddressCacheKey($update));
                $this->deleteMessage($update);
                break;
            default:
                $this->sendTransactionRecord($update);
        }
    }

    public function deleteMessage(Update $update)
    {
        $messageId = $update->callbackQuery->message->messageId;
        $chatId = $update->callbackQuery->message->chat->id;
        $this->bot->deleteMessage([
            'chat_id' => $chatId,
            'message_id' => $messageId,
        ]);
    }


    public function handleText(Update $update)
    {
        $text = $update->getMessage()->text;
        switch ($text) {
            case "🛎钱包交易提醒":
                $tgUid = $update->message->from->id;
                $list = $this->model->newQuery()->where('tg_uid', $tgUid)->get(['tron_address', 'options']);
                if ($list->count() == 0) {
                    $this->bot->sendMessage(
                        [
                            'chat_id' => $update->getChat()->id,
                            'text' => config('bot.binding_text'),
                            'reply_markup' => $this->makeBindingButton(),
                        ]
                    );
                    return;
                }
                $this->sendTgTronList($update);
                break;
            case "💹交易记录查询":
                Cache::put($this->getQueryAddressCacheKey($update), 1, 300);
                $this->bot->sendMessage(
                    [
                        'chat_id' => $update->getChat()->id,
                        'text' => config('bot.send_query_text'),
                        'reply_markup' => $this->makeQueryCancel(),
                    ]
                );
                break;
            case "✅丝绸公群导航":
                $this->bot->sendMessage(
                    [
                        'chat_id' => $update->getChat()->id,
                        'text' => '✅丝绸公群导航: @a444',
                        'reply_markup' => $this->makeKeyboardButton(),
                    ]
                );
                break;
            case "✅丝绸工作人员名单":
                $this->bot->sendMessage(
                    [
                        'chat_id' => $update->getChat()->id,
                        'text' => '✅丝绸之路曝光频道:  @a000
✅丝绸之路灰产主群:  @a111
✅丝绸之路需求频道:  @a222
✅丝绸之路供应频道:  @a333
✅丝绸之路公群导航:  @a444
✅丝绸之路：小无  @a555
✅丝绸之路：阿许  @a666
✅丝绸之路：阿卡  @a777
✅丝绸之路：阿诚  @a888
✅丝绸之路：老路  @a999
✅丝绸之路：仲裁  @zcaa6
✅丝绸之路：阿橙  @aaby6
✅丝绸之路：阿和  @AAGQ6
',
                        'reply_markup' => $this->makeKeyboardButton(),
                    ]
                );
                break;
            default:
                $text = trim($text);
                if (strlen($text) == 34 && Str::startsWith($text, "T")) {
                    $result = $this->tron->validateAddress($text);
                    if (empty($result['result'])) {
                        $this->bot->sendMessage(
                            [
                                'chat_id' => $update->getChat()->id,
                                'text' => '地址有误，请检查',
                                'reply_markup' => $this->makeKeyboardButton(),
                            ]
                        );
                        return;
                    }
                    $this->queryAddress($update);
                    return;
                }
        }
    }

    public
    function queryAddress(Update $update)
    {
//        $address = trim($update->getMessage()->text);
//        $tgUid = $update->getMessage()->from->id;
        $this->sendInquiring($update);
        $this->sendAccountDetail($update);
//        $first = $this->model->newQuery()->where('tron_address', $address)
//            ->where('tg_uid', $tgUid)->first(['id']);
//        if ($first) {
//            $this->sendInquiring($update);
//            $this->sendAccountDetail($update);
//            return;
//        }
//        $result = $this->model->save([
//            'tron_address' => $address,
//            'tg_uid' => $tgUid,
//            'option' => json_decode(config('bot.default_options')),
//        ]);
//        if ($result) {
//            $this->sendBindingSuccess($update);
//        }
    }

    public
    function sendAccountDetail(Update $update)
    {
        $address = trim($update->getMessage()->text);
        $trx = $this->tron->getBalance($address, true);
        $contract = $this->tron->contract("TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t");
        $usdt = $contract->balanceOf($address);
        $tronService = app()->make(TronService::class);
        $exRate = $this->getExchangeRate();
        $txInfo = $tronService->getTrc20TxInfo($address, [
            'contract_address' => 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t',
        ]);
        $rmb = bcmul($usdt, $exRate, 6);
        $url = route('trc20.transaction') . '?' . http_build_query(['address' => $address]);
        $this->bot->sendMessage([
            'chat_id' => $update->getChat()->id,
            'text' => <<<EOF
账户：$address

USDT 查询成功
余额：<b>{$usdt}</b>个USDT  实时汇率：<b>{$exRate}</b>元
价值：<b>{$rmb}</b>元人民币
累计交易：<b>{$txInfo['count']}</b>笔
历史总收入：<b>{$txInfo['income']}</b> USDT
历史总支出：<b>{$txInfo['expenses']}</b> USDT
---  ---  ---
TRX余额查询成功
共有：<b>{$trx}</b>个TRX

EOF,
            'parse_mode' => 'HTML',
            'reply_markup' => $this->makeKeyboardButton($url),
        ]);
    }

    public
    function sendInquiring(Update $update)
    {
        $address = trim($update->getMessage()->text);
        $this->bot->sendMessage([
            'chat_id' => $update->getChat()->id,
            'text' => <<<EOF
正在查询账户：
$address
请稍后
EOF,
        ]);
    }


    private
    function handleCommand(UpdateObject $update)
    {
        $text = $update->getMessage()->text;
        $command = ltrim($text, "/");
        if ($command == "start") {
            $this->bot->sendMessage(
                [
                    'chat_id' => $update->getChat()->id,
                    'text' => config('bot.welcome_text'),
                    'reply_markup' => $this->makeKeyboardButton(),
                ]
            );
            return;
        }

        $tronTgUser = new TronTelegramUser();
        $tgUid = $update->message->chat->id;
        if (Str::startsWith($command, "d") && is_numeric(ltrim($command, "d"))) {
            $num = ltrim($command, "d");
            $user = $tronTgUser->getOneByTgUid($tgUid, $num);
            if (!$user) {
                return;
            }

            $user->delete();
        } else if (Str::startsWith($command, "s") && is_numeric(ltrim($command, "s"))) {
            $num = ltrim($command, "s");
            $user = $tronTgUser->getOneByTgUid($tgUid, $num);
            if (!$user) {
                return;
            }
            $options = json_decode($user['options'], true);
            $options['income'] = $options['income'] == 0 ? 1 : 0;
            $user['options'] = json_encode($options);
            $user->save();
        } else if (Str::startsWith($command, "e") && is_numeric(ltrim($command, "e"))) {
            $num = ltrim($command, "e");
            $user = $tronTgUser->getOneByTgUid($tgUid, $num);
            if (!$user) {
                return;
            }
            $options = json_decode($user['options'], true);
            $options['expenses'] = $options['expenses'] == 0 ? 1 : 0;
            $user['options'] = json_encode($options);
            $user->save();
        } else if (Str::startsWith($command, "b") && is_numeric(ltrim($command, "b"))) {
            $num = ltrim($command, "b");
            $user = $tronTgUser->getOneByTgUid($tgUid, $num);
            if (!$user) {
                return;
            }
            $options = json_decode($user['options'], true);
            $options['balance'] = $options['balance'] == 0 ? 1 : 0;
            $user['options'] = json_encode($options);
            $user->save();
        }
        $this->bot->deleteMessage([
            'chat_id' => $update->message->chat->id,
            'message_id' => $update->message->messageId,
        ]);
        $this->sendTgTronList($update);
    }


    public
    function sendBinding(Update $update)
    {
        $chatId = $update->callbackQuery->message->chat->id;
        Cache::put($this->getBindAddressCacheKey($update), 1, 300);
        $this->bot->sendMessage([
            'chat_id' => $chatId,
            'text' => config('bot.send_binding_text'),
            'reply_markup' => $this->makeBindCancel(),
        ]);
    }


    public
    function sendTgTronList(Update $update)
    {
        $tgUid = $update->callbackQuery ? $update->callbackQuery->from->id : $update->message->from->id;
        $list = $this->model->newQuery()->where('tg_uid', $tgUid)->get(['tron_address', 'options']);
        if ($list->count() == 0) {
            return;
        }
        $text = '';
        $optionsMap = config('bot.options_map');
        $defaultOptions = config('bot.default_options');
        $bindingOptions = config('bot.binding_options');
        for ($i = 0; $i < $list->count(); $i++) {
            $item = $list[$i];
            $text .= ($i + 1) . ".{$item['tron_address']}\n";

            $userOptions = json_decode($item['options'], true);

            foreach ($defaultOptions as $key => $item) {
                $text .= "$optionsMap[$key]：" . (!empty($userOptions[$key]) ? '✅' : '☑️') . "|";
            }
            $text = rtrim($text, "|");
            $text .= "\n";

            foreach ($bindingOptions as $key => $item) {
                $text .= "{$optionsMap[$key]} <strong>/$item$i</strong>|";
            }
            $text = rtrim($text, '|');
            $text .= "\n\n";
        }

//        $text .= "[请点击此处发送 /command1 命令](/command1)";
        $this->bot->sendMessage([
            'chat_id' => $update->getChat()->id,
            'text' => $text,
            'parse_mode' => 'HTML',
            'reply_markup' => $this->makeBindingButton(),
        ]);
    }

    public
    function makeBindingButton(): Keyboard
    {
        $texts = [
            '💎绑定地址' => 'binding',
            '⚔️关闭' => 'delete'
        ];
        $buttons = [];
        foreach ($texts as $key => $val) {
            $buttons[] = Button::make()->setText($key)->setCallbackData($val);
        }
        $keyboard = new Keyboard();
        $keyboard->inline()->row(...$buttons);

        return $keyboard;
    }

    public
    function sendBindingSuccess(Update $update)
    {
        $this->bot->sendMessage([
            'chat_id' => $update->getChat()->id,
            'text' => "✅保存绑定成功！",
            'reply_to_message_id' => $update->getMessage()->messageId,
            'reply_markup' => $this->makeBack(),
        ]);
    }

    public
    function sendBindingFail(Update $update)
    {
        $this->bot->sendMessage([
            'chat_id' => $update->getChat()->id,
            'text' => "⚠️绑定失败",
            'reply_to_message_id' => $update->getMessage()->messageId,
        ]);
    }

    public
    function sendBindingExist(Update $update)
    {
        $this->bot->sendMessage([
            'chat_id' => $update->getChat()->id,
            'text' => "⚠️当前钱包地址已被绑定",
            'reply_to_message_id' => $update->getMessage()->messageId,
        ]);
    }

    public
    function sendBindingUrlInvalid(Update $update)
    {
        $this->bot->sendMessage([
            'chat_id' => $update->getChat()->id,
            'text' => "⚠️无效地址，请确认",
            'reply_to_message_id' => $update->getMessage()->messageId,
        ]);
    }

    public
    function makeKeyboardButtonAddress(string $address): Keyboard
    {
        $texts = [
            '🔍在线查询' => "$address",
        ];
        $inlineButtons = [];
        foreach ($texts as $key => $val) {
            $inlineButtons[] = Button::make()->setText($key)->setCallbackData($val);
        }

        $keyboard = new Keyboard();
        $texts = config("bot.reply_markup_button", []);
        $buttons = [];
        foreach ($texts as $text) {
            $buttons[] = Button::make()->setText($text);
        }
        $keyboard->row(...$buttons)->inline()->row(...$inlineButtons);

        return $keyboard;
    }


    public
    function makeKeyboardButton()
    {
        $items = config("bot.reply_markup_button", []);
        $keyboard = new Keyboard();
        $keyboard->setResizeKeyboard(true);
        foreach ($items as $item) {
            $buttons = [];
            foreach ($item as $v) {
                $button = Button::make()->setText($v);
                $buttons[] = $button;
            }
            $keyboard->row(...$buttons);
        }

        return $keyboard;
    }

    public
    function makeBindCancel(): Keyboard
    {
        $texts = [
            '⚔️取消' => 'delete_bind'
        ];
        $buttons = [];
        foreach ($texts as $key => $val) {
            $buttons[] = Button::make()->setText($key)->setCallbackData($val);
        }
        $keyboard = new Keyboard();
        $keyboard->inline()->row(...$buttons);

        return $keyboard;
    }

    public
    function makeQueryCancel(): Keyboard
    {
        $texts = [
            '⚔️取消' => 'delete_query'
        ];
        $buttons = [];
        foreach ($texts as $key => $val) {
            $buttons[] = Button::make()->setText($key)->setCallbackData($val);
        }
        $keyboard = new Keyboard();
        $keyboard->inline()->row(...$buttons);

        return $keyboard;
    }

    public
    function makeBack(): Keyboard
    {
        $texts = [
            '🔙返回' => 'back'
        ];
        $buttons = [];
        foreach ($texts as $key => $val) {
            $buttons[] = Button::make()->setText($key)->setCallbackData($val);
        }
        $keyboard = new Keyboard();
        $keyboard->inline()->row(...$buttons);

        return $keyboard;
    }

    private
    function handleAddressMessage(UpdateObject $update)
    {
        $bindKey = $this->getBindAddressCacheKey($update);
        $queryKey = $this->getQueryAddressCacheKey($update);
        if (!Cache::get($bindKey) && !Cache::get($queryKey)) {
            return false;
        }
        $address = trim($update->getMessage()->text);
        $tgUid = $update->getMessage()->from->id;
        $result = $this->tron->validateAddress($address);
        if (empty($result['result'])) {
            Cache::forget($bindKey);
            Cache::forget($queryKey);
            $this->sendBindingUrlInvalid($update);
            return true;
        }
        if (Cache::get($bindKey)) {
            $first = $this->model->newQuery()->where('tron_address', $address)->where('tg_uid', $tgUid)->first(['id']);
            if ($first) {
                Cache::forget($bindKey);
                $this->sendBindingExist($update);
                return true;
            }

            $result = $this->model->fill([
                'tron_address' => $address,
                'tg_uid' => $tgUid,
                'options' => json_encode(config('bot.default_options')),
            ])->save();
            Cache::forget($bindKey);
            if (!$result) {
                $this->sendBindingFail($update);
                return true;
            }
            $this->sendBindingSuccess($update);
            return true;
        } else if (Cache::get($queryKey)) {
            Cache::forget($queryKey);
            $this->sendInquiring($update);
            $this->sendTransactionRecord($update);
            return true;
        }
        return false;
    }


    public
    function getExchangeRate()
    {
        $exchangeRate = Cache::get("exchange_rate");
        if ($exchangeRate) {
            return $exchangeRate;
        }
        try {
            $exchangeRate = $this->current('USD', 'CNY');
        } catch (\Throwable $throwable) {
            $exchangeRate = 6.88;
        }
        Cache::put("exchange_rate", $exchangeRate, 7200);
        return $exchangeRate;
    }


    public
    function sendUSDTEvent(string $address)
    {
        $now = (int)now('Asia/Shanghai')->getPreciseTimestamp(3);
        $tronService = app()->make(TronService::class);
        $lastEventTime = Cache::get($address);
        if (!$lastEventTime) {
            $lastEventTime = (int)now('Asia/Shanghai')->subSeconds(60)->getPreciseTimestamp(3);
        }
        try {
            $txInfo = $tronService->getTrc20Transactions($address, [
                'contract_address' => 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t',
                'limit' => 200,
                'min_timestamp' => $lastEventTime,
                'max_timestamp' => $now,
            ]);
            foreach ($txInfo['data'] as $item) {
                if ($item['token_info']['address'] == 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t') {
                    $this->sendEventByTelegram($address, $item);
                }
            }
            Cache::put($address, $now, 120);
        } catch (\Throwable $throwable) {
            Cache::forget($address);
        }
    }

    public
    function sendEventByTelegram($address, $item)
    {
        $users = (new TronTelegramUser())->newQuery()->where("tron_address", $address)->get(['tg_uid', 'options'])->toArray();
        foreach ($users as $user) {
            $options = json_decode($user['options'], true);
            $type = ($item['from'] == $address) ? "支出" : "收入";
            if ($type == "收入" && empty($options['income'])) {
                continue;
            }

            if ($type == "支出" && empty($options['expenses'])) {
                continue;
            }
            $value = TronService::decimalValue($item['value'], $item['token_info']['decimals']);
//            $date = now('Asia/Shanghai')->format('Y-m-d H:i:s');
            $date = Carbon::createFromTimestampMs($item['block_timestamp'], 'Asia/Shanghai')->toDateTimeString();
            $text = <<<TEXT
✅{$type} 通知
 交易时间：{$date}
 收入金额：<b>{$value}</b> USDT转账
 付款地址：
 {$item['from']}
 收款地址：
 {$item['to']}
TEXT;
            if (!empty($options['balance'])) {
                $trx = $this->tron->getBalance($address, true);
                $contract = $this->tron->contract("TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t");
                $usdt = $contract->balanceOf($address);
                $text .= <<<TEXT

---  ---  ---
 USDT余额：<b>{$usdt}</b>
 TRX余额： <b>{$trx}</b>
TEXT;
            }


            $this->bot->sendMessage([
                'chat_id' => $user['tg_uid'],
                'text' => $text,
                'parse_mode' => 'HTML',
                'reply_markup' => $this->makeKeyboardButton(),
            ]);
        }
    }

    public
    function handleTRButton(Update $update)
    {
        $tgUid = $update->callbackQuery ? $update->callbackQuery->from->id : $update->message->from->id;
        $list = $this->model->newQuery()->where('tg_uid', $tgUid)->get(['tron_address', 'options']);
        if ($list->count() == 0) {
            $this->bot->sendMessage(
                [
                    'chat_id' => $update->getChat()->id,
                    'text' => config('bot.binding_text'),
                    'reply_markup' => $this->makeBindingButton(),
                ]
            );
            return;
        }

        $buttons = [];
        foreach ($list as $item) {
            $buttons[] = Button::make()->setText("🏠 " . $item['tron_address'])->setCallbackData($item['tron_address']);
        }
        $keyboard = new Keyboard();
        $keyboard->inline()->row(...$buttons);
        $this->bot->sendMessage([
            'chat_id' => $update->getChat()->id,
            'text' => '绑定地址列表',
            'reply_markup' => $keyboard,
        ]);
    }

    function sendTransactionRecord(Update $update)
    {
        $pageSize = 20;
        if ($update->objectType() == "message") {
            $address = $update->message->text;
            $this->sendPaginateTransactionRecord($update, $address, [
                'max_timestamp' => (int)now('Asia/Shanghai')->getPreciseTimestamp(3),
                'min_timestamp' => 0,
                'page' => 1,
                'page_size' => $pageSize,
            ]);
        } else if ($update->objectType() == "callback_query") {
            $text = $update->callbackQuery->data;
            list($address, $maxTimestamp, $minTimestamp, $page) = explode('-', $text);
            if (strlen($address) == 34 && Str::startsWith($address, "T") && is_numeric($maxTimestamp) && is_numeric($minTimestamp) && is_numeric($page)) {
                try {
                    $this->deleteMessage($update);
                } catch (\Exception $exception) {

                }
                $this->sendPaginateTransactionRecord($update, $address, [
                    'max_timestamp' => (int)$maxTimestamp,
                    'min_timestamp' => (int)$minTimestamp,
                    'page' => $page,
                    'page_size' => $pageSize,
                ]);
            }
        }


    }


    public
    function sendPaginateTransactionRecord(Update $update, string $address, array $options)
    {
        $options['contract_address'] = 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t';
        $data = $this->getTransactionRecordFromCache($address, $options);
        if (empty($data)) {
            $this->bot->sendMessage([
                'chat_id' => $update->getChat()->id,
                'text' => '暂无交易记录',
                'reply_markup' => $this->makeKeyboardButton(),
            ]);
            return;
        }
        $page = empty($options['page']) ? 1 : $options['page'];
        $pageSize = $options['page_size'] ?? 10;
        $list = array_slice($data, ($page - 1) * $pageSize, $pageSize);
        $pages = (int)ceil(count($data) / $pageSize);
        $text = "";
        foreach ($list as $item) {
            if ($item['type'] == "income") {
                $money = "收入金额：{$item['value']}";
            } else {
                $money = "支出金额：{$item['value']}";
            }
            $text .= <<<EOF

$money USDT
对方地址：<code>{$item['other_address']}</code>
交易时间：{$item['time']}
---------------

EOF;
        }

        $prefix = $this->makeCacheKey($address, $options['max_timestamp'] ?? 0, $options['min_timestamp'] ?? 0) . "-";
        if ($page > 1 && $page < $pages) {
            $button = [
                Button::make()->setText("👈 上一页")->setCallbackData($prefix . ($page - 1)),
                Button::make()->setText("👉 下一页")->setCallbackData($prefix . ($page + 1)),
            ];
        } else if ($page == 1) {
            $button = [
                Button::make()->setText("👉 下一页")->setCallbackData($prefix . ($page + 1)),
            ];
        } else {
            $button = [
                Button::make()->setText("👈 上一页")->setCallbackData($prefix . ($page - 1)),
            ];
        }

        $keyboard = new Keyboard();
        $keyboard->inline()->row(...$button);
        $keyboard->setResizeKeyboard(true);
        $this->bot->sendMessage([
            'chat_id' => $update->getChat()->id,
            'text' => $text,
            'parse_mode' => 'HTML',
            'reply_markup' => $keyboard,
        ]);
    }

    public
    function makeCacheKey(string $address, int $maxTimestamp, int $minTimestamp)
    {
        $maxTimestamp = empty($maxTimestamp) ? (int)now('Asia/Shanghai')->getPreciseTimestamp(3) : $maxTimestamp;
        $minTimestamp = $minTimestamp ?: 0;
        return "$address-$maxTimestamp-$minTimestamp";
    }

    public
    function getTransactionRecordFromCache(string $address, array $options = [])
    {
        $key = $this->makeCacheKey($address, $options['max_timestamp'] ?? 0, $options['min_timestamp'] ?? 0);
        $list = Cache::get($key);
        if (!$list) {
            $data = app()->make(TronService::class)->getTrc20TransactionList($address, $options);
            $list = [];
            foreach ($data as $item) {
                $type = "income";
                $otherAddress = $item['from'];
                if ($address == $item['from']) {
                    $type = "expend";
                    $otherAddress = $item['to'];
                }
                $value = TronService::decimalValue($item['value'], $item['token_info']['decimals']);
                $value = $this->format_float($value);
                $time = Carbon::createFromTimestampUTC($item['block_timestamp'] / 1000)->tz('Asia/Shanghai')->format("Y-m-d H:i:s");
                $list[] = [
                    'type' => $type,
                    'other_address' => $otherAddress,
                    'value' => $value,
                    'time' => $time,
                ];
            }
            Cache::put($key, $list, 300);
        }
        return $list;
    }

    function format_float($num)
    {
        if (strpos($num, '.') !== false) {
            // 如果字符串中包含小数点
            $num = rtrim(rtrim($num, '0'), '.');
            if (strpos($num, '.') !== false) {
                return $num;
            }
        }
        return number_format($num, 2, '.', '');
    }

    public function current($base = 'USD', $to = 'CNY')
    {
        $client = new Client();
        $appId = config('bot.current_app_id');
        $response = $client->get("https://openexchangerates.org/api/latest.json?app_id=$appId", [
            'timeout' => 2,
            'connect_timeout' => 2,
        ]);

        $data = $response->getBody();
        $result = json_decode($data, true);

        return $result['rates'][$to] ?? '6.88';
    }
}
