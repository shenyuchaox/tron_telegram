<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TronTelegramUser extends Model
{
    use SoftDeletes;

    protected $table = 'tron_telegram_user';

    protected $guarded = ['id'];


    public function getOneByTgUid(int $tgUid, $offset = 0, $order = "id asc")
    {
        return $this->newQuery()->where('tg_uid', $tgUid)->orderByRaw($order)->offset($offset)->first();
    }
}
